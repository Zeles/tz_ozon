package tz_ozon

import "testing"

type TestCase struct {
	Src    string
	Dest   string
	Result bool
}

func TestCheckOZON(t *testing.T) {
	TestCases := []TestCase{{Src: "12345678", Dest: "1234567", Result: true},
		{Src: "cat", Dest: "cut", Result: true},
		{Src: "d12d", Dest: "dasd", Result: false},
		{Src: "abcdefg", Dest: "abcefg", Result: true},
		{Src: "cat", Dest: "at", Result: true},
		{Src: "tac", Dest: "cat", Result: false},
		{Src: "(00+00)", Dest: "(_0+00)", Result: true},
		{Src: "!", Dest: "|", Result: true},
		{Src: "\t", Dest: " ", Result: true}}
	for _, test := range TestCases {
		f := CheckOZON(test.Src, test.Dest)
		if f != test.Result {
			t.Errorf("Src: %s\t| Dest: %s\t| Expecter Result %t\t| Result: %t\n", test.Src, test.Dest, test.Result, f)
		}
	}
}
