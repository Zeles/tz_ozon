package tz_ozon

import "math"

func CheckOZON(src, dest string) bool {
	srclen := len(src) - 1
	destlen := len(dest) - 1
	if math.Abs(float64(srclen-destlen)) >= 2 {
		return false
	}
	e1 := 0
	e2 := 0
	j := 0
	for i := 0; i < srclen; i++ {
		if src[i] != dest[j] {
			e1++
			if e1 > 1 {
				break
			}
			continue
		}
		j++
	}
	j = destlen
	for i := srclen; i > 0; i-- {
		if src[i] != dest[j] {
			e2++
			if e2 > 1 {
				break
			}
			continue
		}
		j--
	}
	if e1 > 1 && e2 > 1 {
		return false
	} else {
		return true
	}
}
